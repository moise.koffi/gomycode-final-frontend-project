import { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Button, Table } from 'react-bootstrap';

// Configuration de l'URL de base pour les requêtes axios
axios.defaults.baseURL = 'http://localhost:5000';

function App() {
  // États pour stocker les données des étudiants, des nouvelles étudiants, des nouvelles notes, etc.
  const [students, setStudents] = useState([]);
  const [newStudent, setNewStudent] = useState({ firstName: '', lastName: '', studentNumber: '' });
  const [newNote, setNewNote] = useState({ studentId: '', subject: '', score: '' });
  const [showAddStudentModal, setShowAddStudentModal] = useState(false);
  const [showAddNoteModal, setShowAddNoteModal] = useState(false);
  const [selectedStudentId, setSelectedStudentId] = useState('');
  const [editStudentId, setEditStudentId] = useState('');
  const [editNoteId, setEditNoteId] = useState('');

  // Effet pour charger les données des étudiants dès que le composant est monté
  useEffect(() => {
    fetchStudents();
  }, []);

  // Fonction pour récupérer les données des étudiants depuis le serveur
  const fetchStudents = async () => {
    try {
      const response = await axios.get('/students');
      setStudents(response.data.data);
    } catch (error) {
      console.error('Erreur lors de la récupération des étudiants:', error);
    }
  };

  // Fonction pour ajouter un nouvel étudiant
  const addStudent = async () => {
    try {
      await axios.post('/students', newStudent);
      setNewStudent({ firstName: '', lastName: '', studentNumber: '' });
      fetchStudents();
      setShowAddStudentModal(false);
    } catch (error) {
      console.error('Erreur lors de l\'ajout de l\'étudiant:', error);
    }
  };

  // Fonction pour mettre à jour un étudiant existant
  const updateStudent = async () => {
    try {
      await axios.put(`/students/${editStudentId}`, newStudent);
      setNewStudent({ firstName: '', lastName: '', studentNumber: '' });
      fetchStudents();
      setEditStudentId('');
      setShowAddStudentModal(false);
    } catch (error) {
      console.error('Erreur lors de la modification de l\'étudiant:', error);
    }
  };

  // Fonction pour ajouter une nouvelle note
  const addNote = async () => {
    try {
      await axios.post('/notes', { ...newNote, studentId: selectedStudentId });
      setNewNote({ subject: '', score: '' });
      fetchStudents();
      setShowAddNoteModal(false);
    } catch (error) {
      console.error('Erreur lors de l\'ajout de la note:', error);
    }
  };

  // Fonction pour mettre à jour une note existante
  const updateNote = async () => {
    try {
      await axios.put(`/notes/${editNoteId}`, newNote);
      setNewNote({ subject: '', score: '' });
      fetchStudents();
      setEditNoteId('');
      setShowAddNoteModal(false);
    } catch (error) {
      console.error('Erreur lors de la modification de la note:', error);
    }
  };

  // Fonction pour supprimer une note
  const deleteNote = async (noteId) => {
    try {
      await axios.delete(`/notes/${noteId}`);
      fetchStudents();
    } catch (error) {
      console.error('Erreur lors de la suppression de la note:', error);
    }
  };

  // Fonction pour supprimer un étudiant
  const deleteStudent = async (studentId) => {
    try {
      await axios.delete(`/students/${studentId}`);
      fetchStudents();
    } catch (error) {
      console.error('Erreur lors de la suppression de l\'étudiant:', error);
    }
  };

  // Fonction pour gérer le changement de l'étudiant sélectionné dans le formulaire de note
  const handleStudentChange = (e) => {
    setSelectedStudentId(e.target.value);
  };

  // Fonction pour gérer le changement des champs de note
  const handleNoteChange = (e) => {
    setNewNote({ ...newNote, [e.target.name]: e.target.value });
  };

  // Fonction pour gérer la modification d'un étudiant
  const handleEditStudent = (student) => {
    setNewStudent({ firstName: student.firstName, lastName: student.lastName, studentNumber: student.studentNumber });
    setEditStudentId(student._id);
    setShowAddStudentModal(true);
  };

  // Fonction pour gérer la modification d'une note
  const handleEditNote = (note) => {
    setNewNote({ subject: note.subject, score: note.score });
    setEditNoteId(note._id);
    setShowAddNoteModal(true);
  };

  // Rendu de l'application avec les composants Modal et Table pour l'affichage des données
  return (
    <div>
      <h1>Gestion des Notes des Étudiants</h1>

      {/* Bouton pour afficher le formulaire d'ajout/modification d'un étudiant */}
      <Button onClick={() => setShowAddStudentModal(true)}>Ajouter un nouvel étudiant</Button>
      {/* Modal pour l'ajout/modification d'un étudiant */}
      <Modal show={showAddStudentModal} onHide={() => { setEditStudentId(''); setShowAddStudentModal(false) }}>
        <Modal.Header closeButton>
          <Modal.Title>{editStudentId ? 'Modifier un étudiant' : 'Ajouter un nouvel étudiant'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* Formulaire pour saisir les informations d'un étudiant */}
          <input type="text" name="firstName" placeholder="Prénom" value={newStudent.firstName} onChange={(e) => setNewStudent({ ...newStudent, firstName: e.target.value })} />
          <input type="text" name="lastName" placeholder="Nom de famille" value={newStudent.lastName} onChange={(e) => setNewStudent({ ...newStudent, lastName: e.target.value })} />
          <input type="text" name="studentNumber" placeholder="Numéro étudiant" value={newStudent.studentNumber} onChange={(e) => setNewStudent({ ...newStudent, studentNumber: e.target.value })} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => { setEditStudentId(''); setShowAddStudentModal(false) }}>Fermer</Button>
          <Button variant="primary" onClick={editStudentId ? updateStudent : addStudent}>{editStudentId ? 'Modifier Étudiant' : 'Ajouter Étudiant'}</Button>
        </Modal.Footer>
      </Modal>

      {/* Bouton pour afficher le formulaire d'ajout/modification d'une note */}
      <Button onClick={() => setShowAddNoteModal(true)}>Ajouter une note</Button>
      {/* Modal pour l'ajout/modification d'une note */}
      <Modal show={showAddNoteModal} onHide={() => setShowAddNoteModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{editNoteId ? 'Modifier une note' : 'Ajouter une note'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* Formulaire pour saisir les informations d'une note */}
          <select name="studentId" value={selectedStudentId} onChange={handleStudentChange}>
            <option value="">Sélectionner un étudiant</option>
            {students.map(student => (
              <option key={student._id} value={student._id}>{student.firstName} {student.lastName}</option>
            ))}
          </select>
          <input type="text" name="subject" placeholder="Matière" value={newNote.subject} onChange={handleNoteChange} />
          <input type="text" name="score" placeholder="Note" value={newNote.score} onChange={handleNoteChange} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddNoteModal(false)}>Fermer</Button>
          <Button variant="primary" onClick={editNoteId ? updateNote : addNote}>{editNoteId ? 'Modifier Note' : 'Ajouter Note'}</Button>
        </Modal.Footer>
      </Modal>

      {/* Tableau pour afficher la liste des étudiants */}
      <h2>Liste des Étudiants avec leurs Notes</h2>
      <Table responsive>
        <thead>
          <tr>
            <th>Prénom</th>
            <th>Nom de famille</th>
            <th>Numéro étudiant</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {students.map(student => (
            <tr key={student._id}>
              <td>{student.firstName}</td>
              <td>{student.lastName}</td>
              <td>{student.studentNumber}</td>
              <td>
                <Button variant="primary" onClick={() => handleEditStudent(student)}>Modifier Étudiant</Button>
                <Button variant="danger" onClick={() => deleteStudent(student._id)}>Supprimer Étudiant</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* Tableau pour afficher la liste des notes */}
      <h2>Liste des Notes</h2>
      <Table responsive>
        <thead>
          <tr>
            <th>Étudiant</th>
            <th>Matière</th>
            <th>Note</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {students.map(student => (
            student.notes.map(note => (
              <tr key={note._id}>
                <td>{student.firstName} {student.lastName}</td>
                <td>{note.subject}</td>
                <td>{note.score}</td>
                <td>
                  <Button variant="primary" onClick={() => handleEditNote(note)}>Modifier Note</Button>
                  <Button variant="danger" onClick={() => deleteNote(note._id)}>Supprimer Note</Button>
                </td>
              </tr>
            ))
          ))}
        </tbody>
      </Table>

      {/* Tableau pour afficher la liste des étudiants avec leurs notes fusionnées */}
      <h2>Liste des Étudiants avec leurs Notes Fusionnées</h2>
      <Table responsive>
        <thead>
          <tr>
            <th>Étudiant</th>
            <th>Matière</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          {students.map(student => (
            student.notes.map(note => (
              <tr key={note._id}>
                <td>{student.firstName} {student.lastName}</td>
                <td>{note.subject}</td>
                <td>{note.score}</td>
              </tr>
            ))
          ))}
        </tbody>
      </Table>
    </div>
  );
}

export default App;
